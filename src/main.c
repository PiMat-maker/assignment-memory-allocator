#define _DEFAULT_SOURCE
#include <stdio.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void debug (const char* fmt, ...);

static struct block_header* get_block_header(void *data);

static const size_t INIT_HEAP_SIZE = 8192;

void test1(struct block_header* first_block){
    debug("Test 1\n");
    void *data = _malloc(1024);
    if (data == NULL){
        err("Test 1 failed.\nData pointer is null\n");
    }
    debug_heap(stdout, first_block);
    if (first_block->is_free){
        err("Test 1 failed.\nBlock is free\n");
    }
    if (first_block->capacity.bytes != 1024){
        err("Test 1 failed.\nCapacity is not right\n");
    }
    _free(data);
    debug("Test 1 passed.\n\n\n");
}

void test2(struct block_header* first_block){
    debug("Test 2\n");
    void *data1 = _malloc(1024);
    void *data2 = _malloc(2048);
    if (data1 == NULL || data2 == NULL){
        err("Test 2 failed.\nData pointer is null\n");
    }

    _free(data1);
    debug_heap(stdout, first_block);

    struct block_header* block1 = get_block_header(data1);
    struct block_header* block2 = get_block_header(data2);

    if (!block1->is_free){
        err("Test 2 failed.\nBlock 1 is not free");
    }
    if (block2->is_free){
        err("Test 2 failed.\nBlock 2 is free");
    }

    _free(data2);
    debug("Test 2 passed.\n\n\n");
}

void test3(struct block_header* first_block){
    debug("Test 3\n");
    void *data1 = _malloc(1024);
    void *data2 = _malloc(2048);
    void *data3 = _malloc(4096);
    if (data1 == NULL || data2 == NULL || data3 == NULL){
        err("Test 3 failed.\nData pointer is null\n");
    }

    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);

    struct block_header* block1 = get_block_header(data1);
    struct block_header* block3 = get_block_header(data3);

    if (!block1->is_free){
        err("Test 3 failed.\nBlock 1 is not free");
    }
    if (block3->is_free){
        err("Test 3 failed.\nBlock 3 is free");
    }

    if (block1->capacity.bytes != 3072 + offsetof(struct block_header, contents)){
        err("Test 3 failed.\nBlock 1's capacity is not right\n");
    }
    if (block3->capacity.bytes != 4096){
        err("Test 3 failed.\nBlock 3's capacity is not right\n");
    }

    _free(data3);
    debug("Test 3 passed.\n\n\n");
}

void test4(struct block_header* first_block){
    debug("Test 4\n");
    void *data1 = _malloc(4096);
    void *data2 = _malloc(8192);
    void *data3 = _malloc(8192);
    if (data1 == NULL || data2 == NULL || data3 == NULL){
        err("Test 4 failed.\nData pointer is null\n");
    }

    _free(data2);
    debug_heap(stdout, first_block);

    struct block_header* block1 = get_block_header(data1);
    struct block_header* block2 = get_block_header(data2);

    if ((uint8_t*) block1->contents + block1->capacity.bytes != (uint8_t*) block2){
        err("Test 4 failed.\nBlock 2's address is not equal\n");
    }

    _free(data1);
    _free(data3);
    debug("Test 4 passed.\n\n\n");
}

void test5(struct block_header* first_block){
    debug("Test 5\n");
    void *data1 = _malloc(24542);
    if (data1 == NULL){
        err("Test 5 failed.\nData pointer is null\n");
    }

    struct block_header* cur_block = first_block;
    while (cur_block->next != NULL){
        cur_block = cur_block->next;
    }

    void* addr = mmap( (uint8_t*) cur_block->contents + cur_block->capacity.bytes, 20000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, 0, 0);
    debug((void*) cur_block);
    if (addr == NULL || addr == MAP_FAILED){
	err("Test 5 failed.\nAddress is null\n");	
    }
    debug(addr);

    debug_heap(stdout, first_block);
    void *data2 = _malloc(81920);

    

    struct block_header* block2 = get_block_header(data2);

    if ((uint8_t*) block2 == (uint8_t*) cur_block->contents + cur_block->capacity.bytes){
        err("Test 5 failed.\nBlock 2 is placed straight after block 1\n");
    }

    _free(data1);
    _free(data2);
    debug("Test 5 passed.\n\n\n");
}

static struct block_header* get_block_header(void *data){
    return (struct block_header*)((uint8_t*) data - offsetof(struct block_header, contents));
}

int main() {
    debug("Heap init\n");
    void *heap = heap_init(INIT_HEAP_SIZE);
    if (heap == NULL){
        err("Can't init heap\n");
    }
    debug("Heap created\n\n\n");
    test1(heap);
    test2(heap);
    test3(heap);
    test4(heap);
    test5(heap);
    debug("All tests successfully passed!");
    return 0;
}
